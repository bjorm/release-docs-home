= Fedora Downloads
The Fedora docs team
:revnumber: F38
:revdate: 2023-04-18 
[abstract]
____
This section provides guidance on how to select the appropriate installation media for the download. 
____

As already mentioned, the https://fedoraproject.org[Fedora Project website] provides all the Fedora Downloads.
The landing page lists all available Editions, Spins, Labs, etc. as described in xref::getting-started.adoc[Getting started], and links to the proper details pages.
Navigate to the page that covers the Fedora variant you decided to use. 

Now you have to make additional choices as described here.

* the _architecture of the hardware_ to install Fedora on
* the _type of installation media_ that fits your needs best

== Hardware architecture

You have to determine: Which architecture is my computer?

The system architecture is essentially determined by the processor.
Fedora officially supports Intel/AMD __**x86_64**__ and ARM __**aarch64**__ architecture.
Additionally installation media and Fedora support for PowerPC _ppc64le_ and 'big iron' _s390x_ are available - just in case. 

Installing Fedora using a wrong architecture medium is not possible.
Consult your manufacturer's documentation for details on your processor.

If you already use a Linux distribution on the respective computer, you can enter `uname -m` within a terminal to identify your architecture.
If you use another operating system, use your preferred search engine to find out how to identify the architecture on the respective computer.
Alternatively, you can search for your hardware (e.g., processor/cpu model, vendor product number of the computer): the processor/cpu determines the architecture.

If you are not sure about your architecture, you should have a look on the table below: this could help you to indicate what you have.

[[List-Processor_and_Architecture_Types]]
.Processor and architecture types

[options="header"]
|===
|Processor manufacturer and types of computer|Architecture type for Fedora
|Most computers with Intel and AMD processors; Mac computers before Apple M1; most server, workstation, desktop and laptop type computers; some netbooks and tablets|`x86_64`
|Most processors other than Intel and AMD; Mac computers beginning with Apple M1; Raspberry Pi and most comparable devices; most tablets; some netbooks|`ARM® aarch64`
|===

Regarding ARM architecture, Fedora supports only 64-bit architecture, aarch64 or arm8, as of release 37 and newer.
The older 32-bit architecture, armhfp or arm7, is dropped.

== Media Types

Fedora provides 4 different types of installation media  that meet different requirements.


Live images::  Live images ("Live ISO")  are designed to boot the computer and to allow you to preview Fedora before installing it. Instead of booting directly into the installer, a live image loads the same environment you will get after installation.
+
The file type is '__.iso__'. You have to transfer the file to a boot medium, usually an USB stick, or still a CD or DVD.
+
Use a live image to install your favorite system, test Fedora on new hardware, troubleshoot, or share with friends.
+
Fedora Workstation, the Fedora Spins and some Fedora Labs are the only ones provided as live images.


Standard images::  Standard images ("Standard ISO") are designed to boot the computer, as well, but boot directly into the installation environment. They include all files needed for the installation and some offer further choices and configuration options. 
+
The file type is '__.iso__'. You have to transfer the file to a boot medium, usually an USB stick, or still a CD or DVD.
+
Use a standard image to perform an offline installation without any internet connection or the available connection is slow or unstable. 
+
Fedora Server, Fedora IoT, Fedora CoreOS, Fedora Silverblue, and Fedora Kinoite are available as standard images.


Netinstall images::  Netinstall images ("Netinstall ISO") are designed to boot the computer, as well, and boot directly into the installation environment. But they provide just the minimum system files to boot and to connect to the internet. Subsequently the system has to download all files needed for installation from the online Fedora package repositories.
+
The file type is '__.iso__'. You have to transfer the file to a boot medium, usually an USB stick, or still a CD or DVD.
+
Use a netinstall image if the system has a stable and fairly fast Internet connection. The installation already accesses the latest updates. 
+
Netinstall images are currently available only for Fedora Server installations. 

Filesystem disk images:: Disk images provide a _preinstalled and preconfigured_ ready to run file system for a specific runtime environment like a virtual machine, e.g. cloud system, or specific hardware like single board computers (SBC), e.g. Raspberry Pi or its alternatives. 
+
The file type is mostly either '__.raw__' or '__.qcow2__'. You have to use a runtime specific installation program, provided either by Fedora (e.g. in case of SBCs) or the runtime provider (e.g. one of the cloud systems like Amazon AWS).
+
Use a filesystem disk image if you want to use one of the intended runtime environments.

